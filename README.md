# README #

### What is this repository for? ###

This is a IntelliJ IDEA project which contains a Logger class that formats a log message into a one line string:

  <LEVEL> hh:mm:ss.SSS <logger name>.<method name>: <message>

### How do I get set up? ###

* Goto File -> Project structure -> Modules -> Add -> Import Module
* Select SimpleLogger.iml. New module shows up.
* Select the module which depends on SimpleLogger.
* Goto Dependencies tab -> Add
* Select Module dependency -> SimpleLogger

* No configuration
* No dependencies
* No tests


### Who do I talk to? ###

* Repo owner