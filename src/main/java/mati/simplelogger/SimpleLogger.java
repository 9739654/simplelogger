package mati.simplelogger;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.MissingResourceException;
import java.util.logging.ConsoleHandler;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

public class SimpleLogger extends Logger {

	static class SimpleLogFormatter extends Formatter {
		private final DateFormat df = new SimpleDateFormat("hh:mm:ss.SSS");

		@Override
		public String format(LogRecord record) {
			return String.valueOf(record.getLevel()) + '\t' + df.format(new Date(record.getMillis())) + ' ' + record
					.getLoggerName() + "." + record.getSourceMethodName() + ":\t" + formatMessage(record) + '\n';
		}
	}

	private static ConsoleHandler consoleHandler;

	static {
		SimpleLogFormatter formatter = new SimpleLogFormatter();
		consoleHandler = new ConsoleHandler();
		consoleHandler.setFormatter(formatter);
	}

	/**
	 * Protected method to construct a logger for a named subsystem.
	 * <p>
	 * The logger will be initially configured with a null Level
	 * and with useParentHandlers set to true.
	 *
	 * @param name               A name for the logger.  This should
	 *                           be a dot-separated name and should normally
	 *                           be based on the package name or class name
	 *                           of the subsystem, such as java.net
	 *                           or javax.swing.  It may be null for anonymous Loggers.
	 * @param resourceBundleName name of ResourceBundle to be used for localizing
	 *                           messages for this logger.  May be null if none
	 *                           of the messages require localization.
	 * @throws MissingResourceException if the resourceBundleName is non-null and
	 *                                  no corresponding resource can be found.
	 */
	protected SimpleLogger(String name, String resourceBundleName) {
		super(name, resourceBundleName);
	}

	public static Logger getLogger(String name) {
		Logger log = Logger.getLogger(name);
		log.addHandler(consoleHandler);
		log.setUseParentHandlers(false);
		return log;
	}

	public static Logger getLogger(Class<?> className) {
		return getLogger(className.getSimpleName());
	}
}
